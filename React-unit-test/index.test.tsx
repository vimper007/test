

const navigate = jest.fn();

jest.mock("../../../../shared/helpers/api", () => ({
  useApi: jest.fn(),
}));




beforeAll(() => {
    jest.spyOn(router, "useNavigate").mockImplementation(() => navigate);
  });

  beforeEach(() => {
    (useApi as jest.Mock).mockReturnValue({
      getPortAPricing: jest.fn().mockResolvedValue({
        primaryPortPrice: {
          oneTimePrice: 100,
          recurringMonthlyPrice: 200,
        },
        secondaryPortPrice: {
          oneTimePrice: 100,
          recurringMonthlyPrice: 200,
        },
      }),
    });
  });
  afterEach(() => {
    jest.clearAllMocks();
  });